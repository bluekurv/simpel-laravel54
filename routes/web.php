<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//   return view('welcome');
// });

Route::get('/', 'PermohonanController@index');

Route::prefix('api')->group(function() {
  Route::resource('maps', 'MapController');
});

Route::get('/permohonan/{permohonan}', 'PermohonanController@show');
Route::get('/perusahaan/{perusahaan}', 'PerusahaanController@show');