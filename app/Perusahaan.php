<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
  protected $table = 'perusahaan';
  protected $primaryKey = 'perID';

  public function permohonan() {
    return $this->belongsTo('App\Permohonan', 'perID', 'mhnPerID');
  }
}
