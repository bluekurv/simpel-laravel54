<?php

namespace App\Http\Controllers;

use App\Map;
use App\Perusahaan;
use App\Permohonan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Input;

class PermohonanController extends Controller
{
  public function index() {
    return view('mapview', ['izinId' => '0', 'izinData' => '0', 'baseUrl' => url('/'), 'mode' => 'check']);
  }

  public function show($permohonan) {
    $permohonan = Permohonan::find($permohonan);
    if ($permohonan) {
      $data = $permohonan->map;
      $data = $permohonan->perusahaan;
      return view('mapview', ['izinId' => $permohonan->getKey(), 'izinData' => $permohonan, 'baseUrl' => url('/'), 'mode' => 'input']);
      // return response($permohonan, 200);
    } else {
      return redirect('/');
    }
  }
}
