<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perusahaan;

class PerusahaanController extends Controller
{
  public function show(Perusahaan $perusahaan) {
    return $perusahaan;
  }
}
