<?php

namespace App\Http\Controllers;

use App\Map;
use App\Permohonan;
use Illuminate\Http\Request;
use Input;

class MapController extends Controller
{
  public function index() {
    return response(Map::all(), 200);
  }

  public function store(Request $request) {
    return response(Map::create($request->all()));
  }

  public function show(Map $map) {
    return $map;
  }

  public function update(Request $request, Map $map) {
    $map->update($request->all());
    $r = $request->all();
    return response($r, 200);
  }

  public function destroy(Map $map) {
    $map->delete();
    return response(204);
  }
}
