<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permohonan extends Model
{
  protected $table = 'permohonan';
  protected $primaryKey = 'mhnID';

  public function map() {
    return $this->hasOne('App\Map', 'izinId', 'mhnID');
  }

  public function perusahaan() {
    return $this->hasOne('App\Perusahaan', 'perID', 'mhnPerID');
  }
}
