<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
  protected $fillable = [
    'izinId',
    'layerIzin',
    'layerSurvey',
    'layerOverlap',
    'layerClear',
    'layerFile',
    'layerRevision'
  ];

  public function permohonan() {
    return $this->belongsTo('App\Permohonan', 'izinId', 'mhnId');
  }
}
