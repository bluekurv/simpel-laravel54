<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SIMPEL Map View</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <link rel="stylesheet" href="css/app.css"> -->
  </head>
  <body>
    <div id="app">
      <mapview :izin-id="{{ $izinId }}" :izin-data="{{ $izinData }}" base-url="{{ $baseUrl }}" mode="{{ $mode }}"></mapview>
    </div>
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
