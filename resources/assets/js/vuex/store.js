import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
  currentData: '',
  databaseAction: '',
  fullBaseUrl : '',
  layerClear: '',
  layerIzin: '',
  layerOverlap: '',
  layerSurvey: '',
  mapsList: '',
  uploadedLayer: '',
  uploadedPoints: '',
  visibleLayers: ''
}

const mutations = {
  CLEARCURRENTDATA (state) {
    state.currentData = {}
  },
  SETCURRENTDATA (state, data) {
    state.currentData = data
  },
  SETMAPSLIST (state, data) {
    state.mapsList = data
  },
  SETLAYERIZIN (state, data) {
    state.layerIzin = data
  },
  SETLAYERSURVEY (state, data) {
    state.layerSurvey = data
  },
  SETLAYEROVERLAP (state, data) {
    state.layerOverlap = data
  },
  SETLAYERCLEAR (state, data) {
    state.layerClear = data
  },
  SETUPLOADEDLAYER (state, data) {
    state.uploadedLayer = data
  },
  SETUPLOADEDPOINTS (state, data) {
    state.uploadedPoints = data
  },
  SETDATABASEACTION (state, data) {
    state.databaseAction = data
  },
  SETFULLBASEURL (state, data) {
    state.fullBaseUrl = data
  }
}

const getters = {
  getCurrentData: state => {
    return state.currentData
  },
  getMapsList: state => {
    return state.mapsList
  },
  getLayerIzin: state => {
    return state.layerIzin
  },
  getLayerSurvey: state => {
    return state.layerSurvey
  },
  getLayerOverlap: state => {
    return state.layerOverlap
  },
  getLayerClear: state => {
    return state.layerClear
  },
  getUploadedLayer: state => {
    return state.uploadedLayer
  },
  getDatabaseAction: state => {
    return state.databaseAction
  },
  getUploadedPoints: state => {
    return state.uploadedPoints
  },
  getFullBaseUrl: state => {
    return state.fullBaseUrl
  }
}

const actions = {
  clearCurrentData ({commit}) {
    commit('CLEARCURRENTDATA')
  },
  setCurrentData (context, data) {
    context.commit('SETCURRENTDATA', data)
  },
  setMapsList (context, data) {
    context.commit('SETMAPSLIST', data)
  },
  setLayerIzin (context, data) {
    context.commit('SETLAYERIZIN', data)
  },
  setLayerSurvey (context, data) {
    context.commit('SETLAYERSURVEY', data)
  },
  setLayerOverlap (context, data) {
    context.commit('SETLAYEROVERLAP', data)
  },
  setLayerClear (context, data) {
    context.commit('SETLAYERCLEAR', data)
  },
  setUploadedLayer (context, data) {
    context.commit('SETUPLOADEDLAYER', data)
  },
  setDatabaseAction (context, data) {
    context.commit('SETDATABASEACTION', data)
  },
  setUploadedPoints (context, data) {
    context.commit('SETUPLOADEDPOINTS', data)
  },
  setFullBaseUrl (context, data) {
    context.commit('SETFULLBASEURL', data)
  }
}

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})