
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// import axios from 'axios'
import store from './vuex/store';

import Buefy from 'buefy';
import 'buefy/lib/buefy.css';
import 'material-design-icons/iconfont/material-icons.css'

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('mapview', require('./components/simpel-mapview/src/components/MapView.vue')); 
Vue.component('welcome', require('./components/simpel-mapview/src/components/Welcome.vue')); 

Vue.use(Buefy)
// Vue.http = Vue.prototype.$http = axios

const app = new Vue({
    el: '#app',
    store
});
