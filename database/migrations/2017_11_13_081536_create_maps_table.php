<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('izinId')->unique();
            $table->longtext('layerIzin')->nullable();
            $table->longtext('layerSurvey')->nullable();
            $table->longtext('layerOverlap')->nullable();
            $table->longtext('layerClear')->nullable();
            $table->longtext('layerFile')->nullable();
            $table->integer('layerRevision')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maps');
    }
}
