# SIMPEL LARAVEL

## Requirement

1. PHP 5.6 (SIMPEL won't run with PHP > 5.6. Therefore the Laravel version chosen is 5.4 to comply with the PHP version required by SIMPEL.)
2. NodeJS >= 6.xx
3. `yarn` package manager for NodeJS

## Git Clone

`git clone --recurse-submodule https://gitlab.com/bluekurv/simpel-laravel54.git`

`cd simpel-laravel54`

## Install PHP dependencies

`composer install`

## Regenerate Laravel APP KEY

`php artisan key:generate`

## Install Node.JS dependencies

`yarn`

## Edit .env file

`cp .env.example .env`

And edit the file to point the database information to the existing SIMPEL database. Next, run the migration to create the `maps` table.

`php artisan migrate`

Check for errors and revise .env file accordingly.

## Serve and watch for development
Run php server using artisan

`php artisan serve`

In another terminal tab, run webpack watch

`yarn run watch`

## Deployment

TODO